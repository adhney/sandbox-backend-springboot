# Use an official OpenJDK image as the base image
FROM openjdk:17-jdk-slim AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the Gradle build files first to leverage Docker cache
COPY gradlew .
COPY gradle ./gradle
COPY build.gradle .
COPY settings.gradle .

# Download and cache the Gradle wrapper distribution
RUN ./gradlew --version

# Copy the source code into the container
COPY src ./src

# Expose the port that your Spring Boot application is listening on
EXPOSE 8080

# Set the command to run your Spring Boot application with hot reloading using spring-boot:run
CMD ["./gradlew", "bootRun"]
